# Introduction
This repository is a parent repository containing learnings in other repositories as submodules.

You can navigate to the other repositories by clicking on the individual folder. 

# Repositories

## wood-workers-helpers
Independent project of a fictitious company to learn various technologies. 

## angular-udemy-learning
Contains notes and Angular examples on Angular Learnings. 

## kafka-udemy-learning
Contains notes and Java examples on Kafka Learnings.

## mssc-udemy-learning
Contains notes and Spring Boot examples on Spring Cloud and various technologies. 

## designpatterns
Contains notes and simple Java examples on Design Patterns.

## post-board (Old)
Contains RoR and React examples.
